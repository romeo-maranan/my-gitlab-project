FROM node:16-alpine

RUN npm install -g static-server

COPY dist /usr/src/app

WORKDIR /usr/src/app

CMD [ "static-server" ]
